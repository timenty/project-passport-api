const Op  =  require("sequelize").Op;
const User = require('../models').User;
const cryptoHelper = require('../helpers/сrypt');

class Authenticate {
	static async checkExistUserOrCreate(userCredentials){
        let user = await User.findOne({
                where: {
                    [Op.or]: [
                        {email: userCredentials.email},
                        {login: userCredentials.login}
                        ]
                },
                attributes: ['login','email']
            });
        if (user) throw 'user exists';
        user = await Authenticate.createUser(userCredentials);
        return user;
    }
    static async createUser(userCredentials){
        try{
            // солим пароль
            try {
                userCredentials.password = cryptoHelper.hashing(userCredentials.password);
            } catch(e) {
                console.log(e);
                throw e;
            }
            // создаём нового пользователя
            const newUser = await User.create(userCredentials);
            console.log('user created correctly')
            return {
                "role_id": newUser.role_id,
                "id": newUser.id,
                "email": newUser.email,
                "login": newUser.login,
                "createdAt": newUser.createdAt,
            };
        }catch (e) {
            throw e;
        }
    }
    static async findExistsUser(userCredentials){
        try {
            return await User.findOne({
                where: {
                    login: userCredentials.login
                }
            });
        }catch (e) {
            throw e;
        }
    }
    static async checkCredentials(userData){
        try{
            const user = await this.findExistsUser(userData);
            const result = await cryptoHelper.asyncCompare(userData.password, user.password);
            return {"result": result,"user": user};
        }catch (e) {
            throw e;
        }
    }
}

module.exports = Authenticate;