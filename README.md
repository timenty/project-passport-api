### Initialize project
* first step `npm i`
* second step please check configuration in `./config/database.json`
* third step **Migration** run this command `npx sequelize db:migrate --config config/database.json`
* third four **Seeds** run this command `npx sequelize db:seed:all --config config/database.json`
* four step создать в директории config env.json на основе env-example.json
### Use project
`npm start` ??? PROFIT  
`npm test` Для тестировния  
`npm run test-with-coverage` Для тестировния + с отчётом о покрытии кода

Здесь у нас классическая MVC модель  
где в папке  
Models Модели  
Routes роуты  

что интересного и необычного?  
* Здесь есть sequelize cli. Его можно призвать командой `npx sequelize`
* seeders это папка с инсертами в таблицу, для развёртки проекта. Где я добавляю в таблицу базовых юзеров и права



что почитать, чтоб врубиться?
* [Про токены, JSON Web Tokens (JWT), аутентификацию и авторизацию. Token-Based Authentication](https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc)
* [Официальный сайт JWT](https://jwt.io/)  
Аутентификация(authentication – реальный, подлинный) - это процесс проверки учётных данных пользователя (логин/пароль). Проверка подлинности пользователя путём сравнения введённого им логина/пароля с данными сохранёнными в базе данных.  
Авторизация(authorization — разрешение, уполномочивание) - это проверка прав пользователя на доступ к определенным ресурсам.


### Доки по используемым технологиям
* ##### Доки для [express-validator](https://express-validator.github.io/docs/)  
* ##### Список [функций для express-validator](https://www.npmjs.com/package/validator)
* ##### [Express.js](https://expressjs.com/)
* ##### [Sequelize ORM](https://sequelize.org/)
* ##### [bcrypt для хэширования](https://www.npmjs.com/package/bcrypt)
* ##### [Mocha фреймворк для тестирования](mochajs.org/)
* ##### [Chai библиотека для тестирования, юзаем для Mocha](chaijs.com)