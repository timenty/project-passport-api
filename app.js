const express = require('express');
const env = process.env.NODE_ENV || 'development';
// var path = require('path');
// var cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();
/* 
	TODO
	disable in production logger('dev')
*/
// if (env === 'development'){
app.use(logger('dev'));
// }
app.disable('x-powered-by');
app.use(express.json({}));

app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});
// app.use(express.static(path.join(__dirname, 'public'), {index: false}));
app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app;