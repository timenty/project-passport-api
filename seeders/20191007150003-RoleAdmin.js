'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'Roles',
    [
      {
        title: 'Administrator',
        description: 'Admin account',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'User',
        description: 'Admin account',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ],
    {},
  ),
  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Roles', null, {}),
};