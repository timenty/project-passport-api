'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'Users',
    [
      {
        login: 'admin',
        email: 'admin@admin.com',
        password: 'password',
        role_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        login: 'user',
        email: 'user@user.com',
        password: 'password',
        role_id: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ],
    {},
  ),
  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Users', null, {}),
};