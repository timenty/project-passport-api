module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		login: DataTypes.STRING,
		email: {
			type: DataTypes.STRING,
			validate: {
				isEmail: true
			}
		},
		password: DataTypes.STRING,
		role_id: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0
		}
	}, {
		paranoid: true,
		timestamps: true
	});
	User.associate = function(models) {
		models.User.belongsTo(models.Role, {foreignKey: 'role_id'});
	};
	return User;
};