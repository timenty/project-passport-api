module.exports = (sequelize, DataTypes) => {
	const Role = sequelize.define('Role', {
		title: DataTypes.STRING,
		description: DataTypes.TEXT
	}, {
		paranoid: true,
		timestamps: true
	});
	Role.associate = function(models) {
		// associations can be defined here
	};
	return Role;
};