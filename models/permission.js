module.exports = (sequelize, DataTypes) => {
  const Permission = sequelize.define('Permission', {
    path: DataTypes.STRING
  }, {});
  Permission.associate = function(models) {
    // associations can be defined here
  };
  return Permission;
};