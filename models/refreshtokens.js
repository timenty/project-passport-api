'use strict';
module.exports = (sequelize, DataTypes) => {
  const RefreshTokens = sequelize.define('RefreshTokens', {
    uid: DataTypes.INTEGER,
    token: DataTypes.STRING(512),
    useragent: DataTypes.STRING,
    role: DataTypes.INTEGER
  }, {});
  RefreshTokens.associate = function(models) {
    // associations can be defined here
  };
  return RefreshTokens;
};