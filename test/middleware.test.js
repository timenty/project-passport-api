/* library */
const chai = require('chai');
const assert = require('chai').assert;
const should = chai.should();
/* modules */
const permit = require('../middleware/authorization');
const JwtHelper = require('../helpers/jwt');
const jwt = new JwtHelper;
const veryOldToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpZCI6MzMsInVzZXJhZ2VudCI6Imluc29tbmlhLzcuMC4xIiwiY3JlYXRlVGltZXN0YW1wIjoxNTcxNTA1NzkwLCJpYXQiOjE1NzE1MDU3OTAsImV4cCI6MTU3MTY3ODU5MH0.vXznA2NYkTGmoM0cZoD11gpmbQHJp3LqaWAEsiECOSk";
// Данный модуль требует соединения с базой данных
describe('Middleware testing', function(){
    it('JWT fail create token', async() => {
        try{
            const tokenPair = await jwt.createToken({
                /* "role_id": 1, коммитим, что бы вызвать ошибку*/
                // "id": 0
            });
            assert.isNotOk(true); // передаём ложь, т.к create token должен выдать исключение
        }catch (e) {
            assert.isOk(e);
        }
    });
    it('JWT failed update refresh token', async() => {
        try {
            await jwt.update(veryOldToken);
        } catch (e) {
            assert.isOk(e);
        }
    });
    it('JWT create expects true and access test', async () => {
        const tokenPair = await jwt.createToken({
            "role_id": 1,
            "id": 0
        });
        assert.isOk(tokenPair.tokens.access_token);
        assert.isOk(tokenPair.tokens.refresh_token);
    });
    // it('permit test expects true', async () => {
    //     /*
    //     * TODO сэмулировать запрос
    //     * */
    //     let tokenPair = await jwt.createToken({
    //         "role_id": 1,
    //         "id": 0
    //     });
    //     permit('administrator');
    //     assert.isOk(tokenPair.tokens.access_token);
    //     assert.isOk(tokenPair.tokens.refresh_token);
    // });
});
