const cryptoHelper = require('../helpers/сrypt');
const chai = require('chai');
const assert = require('chai').assert;
const should = chai.should();
let password = 'qwerty123';
let hashedPassword = '$2b$10$zMDsaPGK1u6D0onV2FzDU.bkkuJsMWC8Pg3hWCBkLUsdzobopAF1S'; // contain qwerty123

describe('cryptoHelper testing', function(){
    it('password hash is working', function (done) {
        assert.isOk(cryptoHelper.hashing(password));
        done();
    });
    it('password compare should be return false', function (done) {
        assert.isNotOk(cryptoHelper.compare('kek', hashedPassword));
        done();
    });
    it('password compare should be return true', function (done) {
        assert.isOk(cryptoHelper.compare(password, hashedPassword));
        done();
    });
    it('password async compare should be return true', function (done) {
        cryptoHelper.asyncCompare(password, hashedPassword).then(function ($result) {
            done();
        });
    })
});
