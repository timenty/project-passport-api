// const sum = require('./request');
const chai = require('chai');
const assert = require('chai').assert;
const should = chai.should();

function sum(a,b){
    return a+b;
}
describe('Login testing', function(){
    it('adds 1 + 2 to equal 3', function (done) {
        assert.equal(sum(1, 2), 3);
        done();
    });
});
