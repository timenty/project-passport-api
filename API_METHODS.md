Content-Type: application/json
--
Authentications methods
---
#### Registration
**POST** `/registration`  
**BODY** 
```json5
{
    "login": login:string,
    "password": password:string,
    "email": email:string
}
```
##### example body
```json
{
	"login": "UserKek",
	"password": "1234qweqwe",
	"email": "example@gmail.com"
}
```
##### example response
```json
{
  "success": true,
  "data": {
    "user": {
      "role_id": 0,
      "id": 34,
      "email": "sporretimur@gmail.com",
      "login": "Timenty",
      "createdAt": "2019-10-19T17:23:57.448Z"
    },
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpYXQiOjE1NzE1MDU4MzcsImV4cCI6MTU3MTUwOTQzN30.Und7E1zNf6KfMY3_HgyZd6Nu_Sdl3yKLSpaR07wO8ec",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpZCI6MzQsInVzZXJhZ2VudCI6Imluc29tbmlhLzcuMC4xIiwiY3JlYXRlVGltZXN0YW1wIjoxNTcxNTA1ODM3LCJpYXQiOjE1NzE1MDU4MzcsImV4cCI6MTU3MTY3ODYzN30.5LQrHosSenhItVgpSWVs1ExfVaymntqC6UXuAldvMAI"
  },
  "errors": []
}
```

#### Login
**POST** `/login`  
**BODY** 
```json5
{
	"login": login:string,
	"password": password:string
}
```
##### example body
```json
{
	"login": "UserKek",
	"password": "1234qweqwe"
}
```
##### example response
```json
{
   "success": true,
   "data": {
     "user": {
       "role_id": 0,
       "id": 34,
       "email": "sporretimur@gmail.com",
       "login": "Timenty",
       "createdAt": "2019-10-19T17:23:57.000Z"
     },
     "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpYXQiOjE1NzE1MDk1OTIsImV4cCI6MTU3MTUxMzE5Mn0.QLdIG3F6zR-At1xSw1ifd1cn0BzNITuqbeUZC4m28aQ",
     "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpZCI6MzQsInVzZXJhZ2VudCI6Imluc29tbmlhLzcuMC4xIiwiY3JlYXRlVGltZXN0YW1wIjoxNTcxNTA5NTkyLCJpYXQiOjE1NzE1MDk1OTIsImV4cCI6MTU3MTY4MjM5Mn0.ZaAA3nBWJXhy-dXWNQjl0guH6av9FgANOx0JxCrdjoc"
   },
   "errors": []
 }
```

#### Update token
**POST** `/update-token`  
**BODY**
```json5
{
	"refreshToken": refresh_token:string:hash
}
```
##### example response
```json
{
  "success": true,
  "data": {
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpYXQiOjE1NzE1MTIxMjUsImV4cCI6MTU3MTUxNTcyNX0.shIQxjdrvVUZpaU35BG8qJOv03CfuvVASK1-OmoA4Pk",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlX2lkIjowLCJpZCI6MzQsInVzZXJhZ2VudCI6Imluc29tbmlhLzcuMC4xIiwiY3JlYXRlVGltZXN0YW1wIjoxNTcxNTEyMTI1LCJpYXQiOjE1NzE1MTIxMjUsImV4cCI6MTU3MTY4NDkyNX0.3L3sZ40I60uSznyEL9N8ljxzlE-SczB8qhadonDbe_M"
  },
  "errors": []
}
```