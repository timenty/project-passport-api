const { validationResult } = require('express-validator');
const Authenticate = require('../services/authenticate');
const JwtHelper = require('../helpers/jwt');

class AuthenticationController {
	static async registration(req, res){
		try{
			// console.log('registration');
			this.validation(req);
			// console.log('validation ok');
			const UserData = await Authenticate.checkExistUserOrCreate(req.body);
			// console.log('UserData ok');
			const jwt = new JwtHelper;
			const dataAndTokens = await jwt.createToken(
				UserData, req.headers['user-agent']
			);
			// console.log('User agent ok token created');
			res.json({
				"success": true,
				"data": {
					"user": this.preparedUserData(UserData),
					"access_token": dataAndTokens.tokens.access_token,
					"refresh_token": dataAndTokens.tokens.refresh_token
				},
				"errors": []
			});
		}catch (e) {
			this.errorJSON(res, e);
		}
	}
	static async login(req, res){
		try{
			this.validation(req);
			const authResult = await Authenticate.checkCredentials(req.body);
			if (authResult.result) {
				const jwt = new JwtHelper;
				const dataAndTokens = await jwt.createToken(
					authResult.user, req.headers['user-agent']
				);
				res.json({
					"success": true,
					"data": {
						"user": this.preparedUserData(authResult.user),
						"access_token": dataAndTokens.tokens.access_token,
						"refresh_token": dataAndTokens.tokens.refresh_token
					},
					"errors": []
				});
			}else{
				this.errorJSON(res,'failed login');
			}
		}catch (e) {
			this.errorJSON(res, e, 500);
		}
	}
	static async updateToken(req, res){
		try{
			this.validation(req);
			const jwt = new JwtHelper();
			const newTokens = await jwt.update(req.body.refreshToken);
			res.json({
				"success": true,
				"data": {
					"access_token": newTokens.tokens.access_token,
					"refresh_token": newTokens.tokens.refresh_token
				},
				"errors": []
			});
		}catch (e) {
			this.errorJSON(res,e);
		}
	}
	static preparedUserData(UData){
		return {
			"role_id": UData.role_id,
			"id": UData.id,
			"email": UData.email,
			"login": UData.login,
			"createdAt": UData.createdAt,
		}
	}
	static validation(req) {
		const errResult = validationResult(req);
		if (errResult.errors.length) throw errResult.errors;
	}
	static errorJSON(req, e, stat = 401){
		req.status(stat).json({
			"success": false,
			"data": {},
			"errors": [e]
		})
	}
}

module.exports = AuthenticationController;