const bcrypt = require('bcrypt');

module.exports = {
    "hashing": (password) => bcrypt.hashSync(password, 10),
    "compare": (password, hashed_password) => bcrypt.compareSync(password,hashed_password),
    "asyncCompare": (password, hashed_password) => bcrypt.compare(password, hashed_password)
};