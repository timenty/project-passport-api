const jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/env.json')[env];
const RefreshTokens = require('../models/').RefreshTokens;
const User = require('../models').User;

/*
    TODO покрыть тестами этот модуль,
*/
class JwtHelper {
    async createToken(userData = {
        "role_id": 0,
        "id": 0
    }, userAgent = 'default'){
        userData['useragent'] = userAgent;
        let tokens = await this.generateTokens(userData);
        if (!tokens) throw 'не удалось создать токен'; // не удалось создать токен
        return tokens;
    }
    hasTokens(tokens = {
        "access_token": 0,
        "refresh_token": 0
    }){
        return !!(tokens.access_token && tokens.refresh_token);
    }
    async generateTokens(userData) {
        let tokensTemplate = {
                "access_token": jwt.sign({
                    role_id: userData.role_id,
                }, config.access, {
                    expiresIn: '1h'
                }),
                "refresh_token": jwt.sign({
                    role_id: userData.role_id,
                    id: userData.id,
                    useragent: userData.useragent,
                    createTimestamp: (new Date().getTime() / 1000 | 0)
                }, config.refresh, {
                    expiresIn: '2 days'
                })
            };
        if (!this.hasTokens(tokensTemplate)) return 0;
        return await this.store(tokensTemplate, userData);
    }
    async store(tokens, userData){
        let data = await this.updateOrCreate(
            RefreshTokens,
            {
                "uid": userData.id,
                "useragent": userData.useragent
            }, {
            "uid": userData.id,
            "token": tokens.refresh_token,
            "useragent": userData.useragent,
            "role": userData.role_id
        });
        return {"tokens":tokens,"data": data.item};
    }
    async updateOrCreate(model, where, newItem) {
        // https://stackoverflow.com/questions/18304504/create-or-update-sequelize
        // First try to find the record
        const foundItem = await model.findOne({where});
        if (!foundItem) {
            // Item not found, create a new one
            const item = await model.create(newItem);
            return  {item, created: true};
        }
        // Found an item, update it
        const item = await model.update(newItem, {where});
        return {item, created: false};
    }
    async update(refresh_token){
        /*
        * Просроченный токен сам себя выдаст и даст ошибку
        * */
        const errMessage = 'токен не был найден или был просрочен';
        try{
            const decoded = jwt.verify(refresh_token, config.refresh);
            const oldRefreshToken =  await RefreshTokens.findOne({
                "where":{
                    "uid": decoded.id,
                    "useragent": decoded.useragent
                }
            });
            if (!oldRefreshToken) throw errMessage;
            const user = await User.findOne({
                "where":{
                    "id": decoded.id
                }
            });
            user["useragent"] = decoded.useragent;
            return await this.generateTokens(user);
        } catch (e) {
            throw errMessage;
        }
    }
}
module.exports = JwtHelper;