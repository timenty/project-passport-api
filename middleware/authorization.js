const env = process.env.NODE_ENV || 'development';
const jwt = require('jsonwebtoken');
const config = require(__dirname + '/../config/env.json')[env];

function permit(...allowed) {
  const isAllowed = role => allowed.indexOf( Roles.hasOwnProperty(role) ? Roles[role] : 'notPermit') > -1;
  const Roles = {
    1 : "administrator",
    2 : "user",
    0 : "all"
  };
  const errorResponse = (res, e, code = 500 ) => res.status(code).json({"success": false,"err": e});
  // return a middleware
  return (req, res, next) => {
    try {
      const token = req.headers.accesstoken;
      const decodedToken = jwt.verify(token, config.access);
      try{
        if (!isAllowed(decodedToken.role_id)) throw 'Forbidden';
        next();
         // role is allowed, so continue on the next middleware
      } catch (e) { errorResponse(res, e, 500);/* user is forbidden */ }
    } catch (e) { errorResponse(res, e, 401);/* user is forbidden*/ }
  }
}

module.exports = permit;