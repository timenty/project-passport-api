'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'RefreshTokens',
        'useragent',
        {
          type: Sequelize.STRING,
        }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
        'RefreshTokens',
        'useragent'
    );
  }
};