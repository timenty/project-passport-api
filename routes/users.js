const express = require('express');
const router = express.Router();
const Users = require('../models').User;
const Roles = require('../models').Role;
const permit = require('../middleware/authorization');
// permit("administrator", "user") example

/*
* Пора писать контроллер для юзеров :)
* */
/* GET users listing. */
router.get('/all', (req, res, next) => {
	Users.findAll({
		attributes: ['login','email'],
		include: [{
			model: Roles,
			attributes: ['title']
		}]
	}).then(function(data){
		res.json({
			"success": true,
			"data": data
		});
	}, err => { 
		res.json({
			"success": true,
			"data":[]
		});
	});
});

router.get('/:id', (req, res, next) => {
	Users.findOne({
		where: {
			id: req.params.id
		},
		attributes: ['login','email'],
		include: [{
			model: Roles,
			attributes: ['title']
		}]
	}).then(function(data){
		res.json({
			"success": true,
			"data": data
		});
	}, err => { 
		res.json({
			"success": false,
			"data":[]
		});
	});
});

router.post('/delete/:id',
	permit("administrator")
	,(req, res, next) => {
	Users.destroy({	where: {
			id: req.params.id
		},
		force: true
	}).then(function(data){
		res.json({
			"success": true,
			"data": data
		});
	}, err => {
		res.json({
			"success": false,
			"data":[]
		});
	});
});

router.post('/ban/:id',
	permit("administrator")
	,(req, res, next) => {
		Users.destroy({	where: {
				id: req.params.id
			}
		}).then(function(data){
			res.json({
				"success": true,
				"data": data
			});
		}, err => {
			res.json({
				"success": false,
				"data":[]
			});
		});
	});

router.post('/restore/:id',
	permit("administrator")
	,(req, res, next) => {
		Users.restore({	where: {
				id: req.params.id
			}
		}).then(function(data){
			res.json({
				"success": true,
				"data": data
			});
		}, err => {
			res.json({
				"success": false,
				"data":[]
			});
		});
	});

module.exports = router;
