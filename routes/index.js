const express = require('express');
const router = express.Router();
const AuthenticationController = require('../controllers/authentication');
const { check } = require('express-validator');
const permit = require('../middleware/authorization');

/* GET home page. */
router.get('/', function(req, res) {
	res.json({
		"success": true,
		"data":'Hi!'
	});
});

router.post('/registration',
	// Валидация приходящих полей
	[
		check('login')
			.isLength({ min: 4, max: 12 })
			.withMessage('must be at least 4 chars long')
			.isAlpha('en-US')
			.withMessage('must be latin alphabet'),
		check('password')
			.isLength({ min: 8 , max: 12 })
			.withMessage('must be at least 8 chars long'),
		check('email')
			.isEmail()
			.withMessage('email is required'),
	],
	// направляем на контроллер
	async (req, res) => { AuthenticationController.registration(req, res) }
);

router.post('/login',
	[
		check('login')
			.isLength({ min: 4, max: 12 })
			.withMessage('must be at least 4 chars long')
			.isAlpha('en-US')
			.withMessage('must be latin alphabet'),
		check('password')
			.isLength({ min: 8 , max: 12 })
			.withMessage('must be at least 8 chars long')
	],
	async (req, res) => { AuthenticationController.login(req, res) }
);

router.post(
	'/update-token',
	[
		check('refreshToken')
		.isLength({ min: 100 , max: 600 })
		.withMessage('must be full token')
	],
	async (req, res) => {  AuthenticationController.updateToken(req, res) }
);

router.get(
	'/hi',
	permit("administrator", "user", "all"),
	async (req, res) => {
		res.json({
			'hello':'world'
		});
	}
);

module.exports = router;